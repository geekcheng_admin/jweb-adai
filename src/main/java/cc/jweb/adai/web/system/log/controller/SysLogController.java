/*
 * Copyright (c) 2020-2021 imlzw@vip.qq.com jweb.cc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cc.jweb.adai.web.system.log.controller;

import cc.jweb.boot.common.lang.Result;
import cc.jweb.boot.controller.JwebController;
import cc.jweb.boot.db.Db;
import cc.jweb.boot.utils.lang.StringUtils;
import com.jfinal.core.ActionKey;
import cc.jweb.adai.web.system.generator.utils.GeneratorUtils;
import cc.jweb.adai.web.system.sys.model.SysLog;
import io.jboot.web.controller.annotation.RequestMapping;
import cc.jweb.boot.security.annotation.RequiresPermissions;

import java.util.Map;

/**
 * 系统日志
 * 该控制器代码由阿呆代码生成器自动生成
 *
 * @author imlzw
 * @generateDate 2020-09-30 15:07:30
 * @source 阿呆极速开发平台 adai.imlzw.cn
 * @since 1.0
 */
@RequiresPermissions("system:log:view")
@RequestMapping(value = "/system/log", viewPath = "/WEB-INF/views/cc/jweb/web/system/log/gid_5")
public class SysLogController extends JwebController {

    public void index() {
        render("index.html");
    }

    /**
     * 编辑页面
     */
    public void editPage() {
        String id = getPara("log_id");
        SysLog sysLog = null;
        if (id != null) {
            sysLog = SysLog.dao.findById(id);
            if (sysLog == null) {
                sysLog = new SysLog();
            }
        }
        if (sysLog == null) {
            sysLog = new SysLog();
        }
        setAttr("detail", sysLog);
        keepPara();
        render("edit.html");
    }

    /**
     * 加载分页列表数据
     */
    public void list() {
        Map<String, Object> params = getPageParamsPlus();
        params.put("_filters_", GeneratorUtils.parseFilterParams(params));
        Object[] values = getParaValues("values");
        Result result = new Result(false, "未知异常！");
        result.set("code", 400);
        if (StringUtils.isNotBlank(values)) { // formSelects的回显接口
            params.put("log_ids", values);
            result.setListData(Db.find(Db.getSqlPara("gid-5-tno-1-cc.jweb.adai.web.system.log.sys_log.queryPageList", params)));
            result.setSuccess(true);
            result.set("code", 0);
        } else {
            result = Db.paginate("gid-5-tno-1-cc.jweb.adai.web.system.log.sys_log.queryPageList", "gid-5-tno-1-cc.jweb.adai.web.system.log.sys_log.count", params);
            result.set("count", result.get(Result.LIST_TOTAL_KEY));
            result.setSuccess(true);
            result.set("code", 0);
        }
        renderJson(result);
    }

    /**
     * 保存用户信息（新增与修改）
     */
    public void save() {
        SysLog columnModel = getColumnModel(SysLog.class);
        SysLog oldModel = null;
        Object id = columnModel.get("log_id");
        if (id != null) {
            oldModel = SysLog.dao.findById(id);
        }
        if (oldModel != null) { // 编辑
            columnModel.update();
        } else { // 新增
            //#if(!table.primaryField.fieldConfigObject.isAutoIncrease())
            //if(id == null){
            //   columnModel.set("log_id", IDGenerator.nextId("系统日志"));
            //}#end
            columnModel.save();
        }
        renderJson(new Result(true, "保存系统日志信息成功！"));
    }

    /**
     * 删除记录
     */
    public void delete() {
        String[] ids = getParaValues("ids");
        if (ids == null || ids.length <= 0) {
            renderJson(new Result(true, "删除成功！"));
            return;
        }
        boolean b = true;
        for (String id : ids) {
            b = b & SysLog.dao.deleteById(id);
        }
        renderJson(new Result(b, b ? "删除成功！" : "删除失败！"));
    }

    // ---- 字段关联接口 ----

    /**
     * user_id 字段关联表列表接口
     */
    @ActionKey("/system/log/user_id/sql/list")
    public void userIdSqlList() {
        Map<String, Object> params = getPageParamsPlus();
        params.put("_filters_", GeneratorUtils.parseFilterParams(params));
        Object[] values = getParaValues("values");
        Result result = new Result(false, "未知异常！");
        result.set("code", 400);
        if (StringUtils.isNotBlank(values)) { // formSelects的回显接口
            params.put("user_ids", values);
            result.setListData(Db.find(Db.getSqlPara("gid-5-tno-1-cc.jweb.adai.web.system.log.sys_log.user_id_sql_list", params)));
            result.setSuccess(true);
            result.set("code", 0);
        } else {
            result = Db.paginate("gid-5-tno-1-cc.jweb.adai.web.system.log.sys_log.user_id_sql_list", "gid-5-tno-1-cc.jweb.adai.web.system.log.sys_log.user_id_sql_count", params);
            result.set("count", result.get(Result.LIST_TOTAL_KEY));
            result.setSuccess(true);
            result.set("code", 0);
        }
        renderJson(result);
    }

}