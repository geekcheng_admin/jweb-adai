/*
 * Copyright (c) 2020-2021 imlzw@vip.qq.com jweb.cc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cc.jweb.adai.web.system.oper.model;

import cc.jweb.boot.annotation.Column;
import com.jfinal.plugin.activerecord.IBean;
import io.jboot.db.annotation.Table;
import io.jboot.db.model.JbootModel;

import java.util.*;
import java.lang.*;

/**
 * 操作权限
 * 该模型代码由代码生成器自动生成
 *
 * @generateDate 2020-09-27 14:42:02
 * @since 1.0
 * @author imlzw
 * @source 阿呆极速开发平台 adai.imlzw.cn
 */
@Table(tableName = "sys_oper", primaryKey = "oper_id")
public class SysOper extends JbootModel<SysOper> implements IBean {
	private static final long serialVersionUID = -1601188922587L;
	public static final SysOper dao = new SysOper().dao();

    // 排序号
    @Column(field="order_no")
    private Integer orderNo;

    // 创建时间
    @Column(field="create_datetime")
    private Date createDatetime;

    // 状态
    @Column(field="status")
    private Boolean status;

    // 操作路径
    @Column(field="oper_path")
    private String operPath;

    // 操作关键字
    @Column(field="oper_key")
    private String operKey;

    // 权限标识
    @Column(field="permission_key")
    private String permissionKey;

    // 图标
    @Column(field="oper_icon")
    private String operIcon;

    // 操作名称
    @Column(field="oper_name")
    private String operName;

    // 操作类型
    @Column(field="oper_type")
    private Integer operType;

    // 上级操作
    @Column(field="oper_pid")
    private Integer operPid;

    // 操作编号
    @Column(field="oper_id")
    private Integer operId;


    /**
     *  获取排序号
     */
    public Integer getOrderNo(){
        return (Integer)get("order_no");
    }

    /**
     *  设置排序号
     */
    public SysOper setOrderNo(Integer orderNo) {
        set("order_no", orderNo);
        return this;
    }

    /**
     *  获取创建时间
     */
    public Date getCreateDatetime(){
        return (Date)get("create_datetime");
    }

    /**
     *  设置创建时间
     */
    public SysOper setCreateDatetime(Date createDatetime) {
        set("create_datetime", createDatetime);
        return this;
    }

    /**
     *  获取状态
     */
    public Boolean getStatus(){
        Object value = get("status");
        if(value instanceof Integer){
            Integer b = (Integer)get("status");
            return b != null && b == 1;
        }
        return (Boolean)value;
    }

    /**
     *  设置状态
     */
    public SysOper setStatus(Boolean status) {
        set("status", status ? 1 : 0) ;
        return this;
    }

    /**
     *  获取操作路径
     */
    public String getOperPath(){
        return (String)get("oper_path");
    }

    /**
     *  设置操作路径
     */
    public SysOper setOperPath(String operPath) {
        set("oper_path", operPath);
        return this;
    }

    /**
     *  获取操作关键字
     */
    public String getOperKey(){
        return (String)get("oper_key");
    }

    /**
     *  设置操作关键字
     */
    public SysOper setOperKey(String operKey) {
        set("oper_key", operKey);
        return this;
    }

    /**
     *  获取权限标识
     */
    public String getPermissionKey(){
        return (String)get("permission_key");
    }

    /**
     *  设置权限标识
     */
    public SysOper setPermissionKey(String permissionKey) {
        set("permission_key", permissionKey);
        return this;
    }

    /**
     *  获取图标
     */
    public String getOperIcon(){
        return (String)get("oper_icon");
    }

    /**
     *  设置图标
     */
    public SysOper setOperIcon(String operIcon) {
        set("oper_icon", operIcon);
        return this;
    }

    /**
     *  获取操作名称
     */
    public String getOperName(){
        return (String)get("oper_name");
    }

    /**
     *  设置操作名称
     */
    public SysOper setOperName(String operName) {
        set("oper_name", operName);
        return this;
    }

    /**
     *  获取操作类型
     */
    public Integer getOperType(){
        return (Integer)get("oper_type");
    }

    /**
     *  设置操作类型
     */
    public SysOper setOperType(Integer operType) {
        set("oper_type", operType);
        return this;
    }

    /**
     *  获取上级操作
     */
    public Integer getOperPid(){
        return (Integer)get("oper_pid");
    }

    /**
     *  设置上级操作
     */
    public SysOper setOperPid(Integer operPid) {
        set("oper_pid", operPid);
        return this;
    }

    /**
     *  获取操作编号
     */
    public Integer getOperId(){
        return (Integer)get("oper_id");
    }

    /**
     *  设置操作编号
     */
    public SysOper setOperId(Integer operId) {
        set("oper_id", operId);
        return this;
    }

}