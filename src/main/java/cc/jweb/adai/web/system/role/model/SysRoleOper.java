/*
 * Copyright (c) 2020-2021 imlzw@vip.qq.com jweb.cc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cc.jweb.adai.web.system.role.model;

import cc.jweb.boot.annotation.Column;
import io.jboot.db.annotation.Table;

/**
 * 角色权限
 * 该模型代码由代码生成器自动生成
 *
 * @author imlzw
 * @generateDate 2020-09-30 11:37:28
 * @source 阿呆极速开发平台 adai.imlzw.cn
 * @since 1.0
 */
@Table(tableName = "sys_role_oper", primaryKey = "ro_id")
public class SysRoleOper extends com.jfinal.plugin.activerecord.Model<SysRoleOper> {
    public static final SysRoleOper dao = new SysRoleOper().dao();
    private static final long serialVersionUID = -1601437048482L;

    // 操作编号
    @Column(field="oper_id")
    private Integer operId;

    // 角色编号
    @Column(field="role_id")
    private Integer roleId;

    // 角色权限编号
    @Column(field="ro_id")
    private String roId;

    /**
     * 获取操作编号
     */
    public Integer getOperId() {
        return (Integer) get("oper_id");
    }

    /**
     * 设置操作编号
     */
    public SysRoleOper setOperId(Integer operId) {
        set("oper_id", operId);
        return this;
    }

    /**
     * 获取角色编号
     */
    public Integer getRoleId() {
        return (Integer) get("role_id");
    }

    /**
     * 设置角色编号
     */
    public SysRoleOper setRoleId(Integer roleId) {
        set("role_id", roleId);
        return this;
    }

    /**
     * 获取角色权限编号
     */
    public String getRoId() {
        return (String) get("ro_id");
    }

    /**
     * 设置角色权限编号
     */
    public SysRoleOper setRoId(String roId) {
        set("ro_id", roId);
        return this;
    }

}