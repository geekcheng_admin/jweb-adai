/*
 * Copyright (c) 2020-2021 imlzw@vip.qq.com jweb.cc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cc.jweb.adai.web.system.sys.service;

import java.util.Date;
import java.util.Map;

import cc.jweb.adai.web.system.org.model.SysUser;
import cc.jweb.boot.common.lang.Result;
import cc.jweb.boot.utils.security.PasswordCryptoTool;
import cc.jweb.boot.db.Db;
import org.apache.commons.lang3.StringUtils;

import com.jfinal.plugin.activerecord.Record;

public class SystemUserService {

	public static final SystemUserService service = new SystemUserService();

	/**
	 * 查询分页
	 * @param params
	 * @return
	 */
	public Result paginate(Map<String,Object> params){
		Result result = Db.paginate("system.user.queryPageList","system.user.count",params);
		result.set("code", 0);
		return result;
	}

	public Record getByAccount(String account){
		return Db.findFirst(Db.getSqlPara("system.user.getByAccount", account));
	}

	public Record getByPhone(String phone){
		return Db.findFirst(Db.getSqlPara("system.user.getByPhone", phone));
	}

	public Result add(SysUser user, Integer[] role_id){
		/* 判断手机、帐号是否存在 */
		Record temp = Db.findFirst(Db.getSqlPara("system.user.getByAccount", user.getStr("user_account")));
		if(temp!=null){
			return new Result(false,"帐号已存在");
		}
		if(StringUtils.isNotBlank(user.getStr("user_phone"))){
			temp = Db.findFirst(Db.getSqlPara("system.user.getByPhone", user.getStr("user_phone")));
			if(temp!=null){
				return new Result(false,"手机已存在");
			}
		}
		user.set("user_create_datetime", new Date());
		/* 密码加密 */
		user.set("user_password", PasswordCryptoTool.encryptPassword(user.getStr("user_password")));
		user.save();
		return new Result(true,"操作成功");
	}

	public Result edit(SysUser user, Integer[] role_id){
		SysUser _user = SysUser.dao.findById(user.getInt("user_id"));
		Record temp = null;
		/* 手机 */
		if(StringUtils.isNotBlank(user.getStr("user_phone"))){
			if(!_user.getStr("user_phone").equals(user.getStr("user_phone"))){
				temp = Db.findFirst(Db.getSqlPara("system.user.getByPhone", user.getStr("user_phone")));
				if(temp!=null){
					return new Result(false,"手机已存在");
				}
			}
		}
		/* 密码 */
		if(StringUtils.isNotBlank(user.getStr("user_password"))){
			user.set("user_password", PasswordCryptoTool.encryptPassword(user.getStr("user_password")));
		}else{
			user.set("user_password", _user.getStr("user_password"));
		}
		user.update();
		return new Result(true,"操作成功");
	}

	public Result delete(int user_id){
		SysUser user = SysUser.dao.findById(user_id);
		Db.update(Db.getSqlPara("system.user.delete", user_id));
		return new Result(true,"操作成功");
	}

	public Result status(int user_id){
		SysUser user = SysUser.dao.findById(user_id);
		if(user!=null){
			user.set("user_status", user.getInt("user_status")==0?1:0);
		}
		Db.update("sys_user","user_id", user.toRecord());
		return new Result(true,"操作成功");
	}

	public Result resetpwd(int user_id){
		SysUser user = SysUser.dao.findById(user_id);
		String code_number = ((int)((Math.random()*9+1)*100000))+"";
		Db.update("update sys_user set user_password = ? where user_id = ?",PasswordCryptoTool.encryptPassword(code_number),user_id);
		return new Result(true,"操作成功").set("data", code_number);
	}
}
