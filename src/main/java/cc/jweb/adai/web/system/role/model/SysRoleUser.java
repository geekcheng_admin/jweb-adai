/*
 * Copyright (c) 2020-2021 imlzw@vip.qq.com jweb.cc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cc.jweb.adai.web.system.role.model;

import cc.jweb.boot.annotation.Column;
import com.jfinal.plugin.activerecord.IBean;
import io.jboot.db.annotation.Table;
import io.jboot.db.model.JbootModel;

/**
 * 角色人员
 * 该模型代码由代码生成器自动生成
 *
 * @author imlzw
 * @generateDate 2020-09-30 11:37:44
 * @source 阿呆极速开发平台 adai.imlzw.cn
 * @since 1.0
 */
@Table(tableName = "sys_role_user", primaryKey = "ru_id")
public class SysRoleUser extends JbootModel<SysRoleUser> implements IBean {
    public static final SysRoleUser dao = new SysRoleUser().dao();
    private static final long serialVersionUID = -1601437064458L;

    // 人员编号
    @Column(field = "user_id")
    private Integer userId;

    // 角色编号
    @Column(field = "role_id")
    private Integer roleId;

    // 角色人员编号
    @Column(field = "ru_id")
    private Integer ruId;

    /**
     * 获取人员编号
     */
    public Integer getUserId() {
        return (Integer) get("user_id");
    }

    /**
     * 设置人员编号
     */
    public SysRoleUser setUserId(Integer userId) {
        set("user_id", userId);
        return this;
    }

    /**
     * 获取角色编号
     */
    public Integer getRoleId() {
        return (Integer) get("role_id");
    }

    /**
     * 设置角色编号
     */
    public SysRoleUser setRoleId(Integer roleId) {
        set("role_id", roleId);
        return this;
    }

    /**
     * 获取角色人员编号
     */
    public Integer getRuId() {
        return (Integer) get("ru_id");
    }

    /**
     * 设置角色人员编号
     */
    public SysRoleUser setRuId(Integer ruId) {
        set("ru_id", ruId);
        return this;
    }

}