/*
 * Copyright (c) 2020-2021 imlzw@vip.qq.com jweb.cc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cc.jweb.adai.web.system.generator.service.converter.type;

import cc.jweb.adai.web.system.generator.model.vo.Config;
import cc.jweb.boot.utils.lang.StringUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 类型转换工厂
 *
 * @author linzhiwei
 */
public class ConverterFactory {
    final public static String TEXT_TO_DATE = "text2date";
    final public static String STRING_TO_DATE = "string2date";
    final public static String DATE_TO_TEXT = "date2text";
    final public static String DATE_TO_STRING = "date2string";
    final public static String DEFAULT = "default";
    private static Map<String, Converter> factory = null;
    /**
     * 类型映射
     */
    private static Map<String, Class> clazzMapping = null;

    public static Map<String, Converter> getConverterFactory() {
        if (factory == null) {
            factory = new HashMap<String, Converter>();
            factory.put(TEXT_TO_DATE, new String2DateConverter());
            factory.put(STRING_TO_DATE, new String2DateConverter());
            factory.put(DATE_TO_TEXT, new Date2StringConverter());
            factory.put(DATE_TO_STRING, new Date2StringConverter());

        }
        return factory;
    }

    /**
     * 添加类型转换器
     *
     * @param orgType
     * @param targetType
     * @param converter
     * @return Converter 返回之前设置的转换器，如果没有则返回空
     */
    public static Converter setConverter(String orgType, String targetType, Converter converter) {
        Converter orgConverter = null;
        if (!StringUtils.isBlank(orgType) && !StringUtils.isBlank(targetType) && converter != null) {
            orgConverter = getConverterFactory().put(orgType.toLowerCase() + "2" + targetType.toLowerCase(), converter);
        }
        return orgConverter;
    }

    public static Converter getConverter(String orgType, String targetType) {
        String key = "default";
        Converter converter = null;
        if (!StringUtils.isBlank(orgType) && !StringUtils.isBlank(targetType)) {
            key = orgType.toLowerCase() + "2" + targetType.toLowerCase();
            converter = getConverterFactory().get(key);
        }
        if (converter == null) {
            converter = getConverterFactory().get("default");
        }
        return converter;
    }

    @SuppressWarnings("deprecation")
    public static Object convert(String orgType, String targetType,
                                 Object orgValue, Config config) throws Exception {
        Object returnValue = null;
        if (!StringUtils.isBlank(orgType) && !StringUtils.isBlank(targetType) && orgValue != null) {
//			//类型不变
//			if(orgType.toLowerCase().equals(targetType.toLowerCase())){
//				return orgValue;
//			}
//			//类型改变
//			else{
            Converter converter = getConverter(orgType, targetType);
            if (converter != null) {
                returnValue = converter.convert(orgValue, config);
            } else {
                if ("string".equals(targetType.toLowerCase()) || "text".equals(targetType.toLowerCase())) {
                    if (orgValue instanceof String) {
                        return orgValue;
                    }
                    returnValue = orgValue.toString();
                } else if ("date".equals(targetType.toLowerCase())) {
                    if (orgValue instanceof Date) {
                        return orgValue;
                    }
                    returnValue = Date.parse(orgValue.toString());
                } else if ("int".equals(targetType.toLowerCase()) || "integer".equals(targetType.toLowerCase())) {
                    if (orgValue instanceof Integer) {
                        return orgValue;
                    }
                    returnValue = Integer.parseInt(orgValue.toString());
                } else if ("long".equals(targetType.toLowerCase())) {
                    if (orgValue instanceof Long) {
                        return orgValue;
                    }
                    returnValue = Long.parseLong(orgValue.toString());
                } else if ("float".equals(targetType.toLowerCase())) {
                    if (orgValue instanceof Float) {
                        return orgValue;
                    }
                    returnValue = Float.parseFloat(orgValue.toString());
                } else if ("double".equals(targetType.toLowerCase())) {
                    if (orgValue instanceof Double) {
                        return orgValue;
                    }
                    returnValue = Double.parseDouble(orgValue.toString());
                }
            }
        }
//		}
        return returnValue;
    }
}
