/*
 * Copyright (c) 2020-2021 imlzw@vip.qq.com jweb.cc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cc.jweb.adai.web.system.generator.service.dataprocess;

import cc.jweb.boot.utils.lang.StringUtils;
import cc.jweb.adai.web.system.generator.service.dataprocess.impl.DefaultDataProcess;

import java.util.HashMap;
import java.util.Map;


/**
 * 数据处理工厂
 *
 * @author linzhiwei
 */
public class DataProcessFactory {

    private static Map<String, DataProcess> cache = null;

    public static Map<String, DataProcess> getDataCache() {
        if (cache == null) {
            cache = new HashMap<String, DataProcess>();
            cache.put("default", new DefaultDataProcess());
        }
        return cache;
    }

    public static DataProcess getDataProcess(String className) {
        DataProcess dataProcess = null;
        if (StringUtils.isNotBlank(className)) {
            dataProcess = getDataCache().get(className);
            if (dataProcess == null) {
                try {
                    Class clazz = Class.forName(className);
                    Object instance = clazz.newInstance();
                    dataProcess = (DataProcess) instance;
                    getDataCache().put(className, dataProcess);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
//		if(dataProcess == null){
//			dataProcess = getDataCache().get("default");
//		}
        return dataProcess;
    }
}
