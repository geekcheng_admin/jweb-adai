/*
 * Copyright (c) 2020-2021 imlzw@vip.qq.com jweb.cc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cc.jweb.adai.web.system.generator.service.dataprocess.impl;

import cc.jweb.adai.web.system.generator.model.vo.ConfigElement;
import cc.jweb.adai.web.system.generator.service.dataprocess.DataProcess;
import cc.jweb.adai.web.system.generator.model.vo.Config;

import java.util.List;

/**
 * html过滤插件
 *
 * @author linzhiwei
 */
public class HTMLFilter implements DataProcess {

    @SuppressWarnings("unchecked")
    @Override
    public String process(Object object, Config config) throws Exception {
        String filterString = null;
        if (object != null) {
//			filterString = StringUtils.extractText(object.toString());
        }
        return object.toString();
    }

    @Override
    public String getConfigDesc() {
        return "HTML过滤";
    }

    @Override
    public List<ConfigElement> getConfigParams() {
        return null;
    }

}
