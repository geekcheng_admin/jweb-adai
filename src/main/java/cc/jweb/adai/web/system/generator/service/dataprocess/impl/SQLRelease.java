/*
 * Copyright (c) 2020-2021 imlzw@vip.qq.com jweb.cc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cc.jweb.adai.web.system.generator.service.dataprocess.impl;

import cc.jweb.adai.web.system.generator.model.vo.ConfigElement;
import cc.jweb.adai.web.system.generator.service.dataprocess.DataProcess;
import cc.jweb.boot.utils.lang.StringUtils;
import cc.jweb.adai.web.system.generator.model.vo.Config;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * 表关联处理
 *
 * @author 志伟
 */
public class SQLRelease implements DataProcess {

    private List<ConfigElement> configElementList = null;
    private StringSpliter stringSpliter = null;
    private StringRegexReplace stringReplace = null;

    public SQLRelease() {
        configElementList = new ArrayList<ConfigElement>();
        stringSpliter = new StringSpliter();
        stringReplace = new StringRegexReplace();
        configElementList.addAll(stringSpliter.getConfigParams());
        configElementList.addAll(stringReplace.getConfigParams());
    }

    public static void main(String[] args) {
        Config config = new Config();
        config.addConfig(new ConfigElement("", StringSpliter.SPLIT_REGEX, "[,]"));
        config.addConfig(new ConfigElement("", StringRegexReplace.MATCH_REGEX, "(.*):(.*):(.*)"));
        config.addConfig(new ConfigElement("", StringRegexReplace.REPLACEMENT, "$2"));
        try {
            List<String> process = new SQLRelease().process("aaaa:bbbb:cccc,dddd:eeee:ffff", config);
            System.out.println(process);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> process(Object object, Config config) throws Exception {
        List<String> process = stringSpliter.process(object, config);
        if (process != null) {
            for (int i = 0; i < process.size(); i++) {
                process.set(i, stringReplace.process(process.get(i), config));
            }
        }
        //排空排重
        if (StringUtils.isNotBlank(process)) {
            List<String> newResult = new ArrayList<String>();
            HashSet<String> set = new HashSet<String>();
            for (String str : process) {
                if (str != null && !set.contains(str)) {
                    newResult.add(str);
                    set.add(str);
                }
            }
            return newResult;
        }
        return null;
    }

    public String getConfigDesc() {
        return "/*字符串正则分割再替换数据处理类，前分割后，每个字符串再替换处理！*/";
    }

    public List<ConfigElement> getConfigParams() {
        return configElementList;
    }
}
