/*
 * Copyright (c) 2020-2021 imlzw@vip.qq.com jweb.cc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cc.jweb.adai.web.system.org.controller;

import cc.jweb.boot.controller.JwebController;
import cc.jweb.boot.security.annotation.RequiresPermissions;
import io.jboot.web.controller.annotation.RequestMapping;

/**
 * 标准双模型列表模板主页
 * 该控制器代码由阿呆代码生成器自动生成
 *
 * @generateDate 2020-12-28 18:45:27
 * @since 1.0
 * @author imlzw
 * @source 阿呆极速开发平台 adai.jweb.cc
 */
@RequiresPermissions("generator:13:view")
@RequestMapping(value = "/system/org", viewPath = "/WEB-INF/views/cc/jweb/jweb/web/system/org/gid_13")
public class IndexController extends JwebController {

    public void index() {
        render("index.html");
    }

    public void rightPage() {
        keepPara();
        render("rightPage.html");
    }
}