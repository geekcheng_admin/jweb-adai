/*
 * Copyright (c) 2020-2021 imlzw@vip.qq.com jweb.cc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cc.jweb.adai.web.system.file.controller;

import cc.jweb.adai.web.system.file.model.SysFile;
import cc.jweb.adai.web.system.log.service.SysLogService;
import cc.jweb.adai.web.system.sys.model.SysLog;
import cc.jweb.boot.common.lang.Result;
import cc.jweb.boot.controller.JwebController;
import cc.jweb.boot.db.Db;
import cc.jweb.boot.utils.lang.StringUtils;
import cc.jweb.adai.web.system.generator.utils.GeneratorUtils;
import io.jboot.web.controller.annotation.RequestMapping;
import cc.jweb.boot.security.annotation.RequiresPermissions;

import java.util.Map;

/**
 * 文件
 * 该控制器代码由阿呆代码生成器自动生成
 *
 * @author imlzw
 * @generateDate 2020-10-30 15:35:01
 * @source 阿呆极速开发平台 adai.imlzw.cn
 * @since 1.0
 */
@RequiresPermissions("generator:19:view")
@RequestMapping(value = "/system/fileMgr", viewPath = "/WEB-INF/views/cc/jweb/web/system/file/gid_19")
public class SysFileController extends JwebController {

    public void index() {
        render("index.html");
    }

    /**
     * 加载分页列表数据
     */
    @RequiresPermissions("generator:19:list")
    public void list() {
        Map<String, Object> params = getPageParamsPlus();
        params.put("_filters_", GeneratorUtils.parseFilterParams(params));
        Object[] values = getParaValues("values");
        Result result = new Result(false, "未知异常！");
        result.set("code", 400);
        if (StringUtils.isNotBlank(values)) { // formSelects的回显接口
            params.put("_values_", values);
            String valueKey = getPara("valueKey");
            params.put("_valueKey_", valueKey != null ? valueKey : "file_id");
            result.setListData(Db.find(Db.getSqlPara("gid-19-tno-1-cc.jweb.adai.web.system.file.sys_file.queryPageList", params)));
            result.setSuccess(true);
            result.set("code", 0);
        } else {
            result = Db.paginate("gid-19-tno-1-cc.jweb.adai.web.system.file.sys_file.queryPageList", "gid-19-tno-1-cc.jweb.adai.web.system.file.sys_file.count", params);
            result.set("count", result.get(Result.LIST_TOTAL_KEY));
            result.setSuccess(true);
            result.set("code", 0);
        }
        renderJson(result);
    }

    /**
     * 删除记录
     */
    @RequiresPermissions("generator:19:del")
    public void delete() {
        String[] ids = getParaValues("ids");
        if (ids == null || ids.length <= 0) {
            renderJson(new Result(true, "删除成功！"));
            return;
        }
        boolean b = true;
        for (String id : ids) {
            b = b & SysFile.dao.deleteById(id);
        }
        SysLogService.service.setSyslog(b ? SysLog.STATUS_SUCCESS : SysLog.STATUS_FAILURE, "删除文件信息【id:" + StringUtils.join(ids, ",") + "】" + (b ? "成功" : "失败") + " !");
        renderJson(new Result(b, b ? "删除成功！" : "删除失败！"));
    }

}