/*
 * Copyright (c) 2020-2021 imlzw@vip.qq.com jweb.cc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cc.jweb.adai.web.system.role.controller;

import cc.jweb.adai.web.system.generator.utils.GeneratorUtils;
import cc.jweb.adai.web.system.log.service.SysLogService;
import cc.jweb.adai.web.system.role.model.SysRoleUser;
import cc.jweb.adai.web.system.sys.model.SysLog;
import cc.jweb.boot.common.lang.Result;
import cc.jweb.boot.controller.JwebController;
import cc.jweb.boot.db.Db;
import cc.jweb.boot.security.annotation.RequiresPermissions;
import cc.jweb.boot.utils.lang.StringUtils;
import com.jfinal.core.ActionKey;
import io.jboot.web.controller.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 角色人员
 * 该控制器代码由阿呆代码生成器自动生成
 *
 * @author imlzw
 * @generateDate 2020-09-30 15:15:11
 * @source 阿呆极速开发平台 adai.imlzw.cn
 * @since 1.0
 */
@RequiresPermissions("system:role:view")
@RequestMapping(value = "/system/role/table2", viewPath = "/WEB-INF/views/cc/jweb/web/system/role/gid_17")
public class Table2SysRoleUserController extends JwebController {

    public void index() {
        render("index.html");
    }

    /**
     * 编辑页面
     */
    public void editPage() {
        String id = getPara("ru_id");
        SysRoleUser sysRoleUser = null;
        if (id != null) {
            sysRoleUser = SysRoleUser.dao.findById(id);
            if (sysRoleUser == null) {
                sysRoleUser = new SysRoleUser();
            }
        }
        if (sysRoleUser == null) {
            sysRoleUser = new SysRoleUser();
        }
        setAttr("detail", sysRoleUser);
        keepPara();
        render("table2edit.html");
    }

    /**
     * 加载分页列表数据
     */
    public void list() {
        Map<String, Object> params = getPageParamsPlus();
        params.put("_filters_", GeneratorUtils.parseFilterParams(params));
        Object[] values = getParaValues("values");
        Result result = new Result(false, "未知异常！");
        result.set("code", 400);
        if (StringUtils.isNotBlank(values)) { // formSelects的回显接口
            params.put("ru_ids", values);
            result.setListData(Db.find(Db.getSqlPara("gid-17-tno-2-cc.jweb.adai.web.system.role.sys_role_user.queryPageList", params)));
            result.setSuccess(true);
            result.set("code", 0);
        } else {
            result = Db.paginate("gid-17-tno-2-cc.jweb.adai.web.system.role.sys_role_user.queryPageList", "gid-17-tno-2-cc.jweb.adai.web.system.role.sys_role_user.count", params);
            result.set("count", result.get(Result.LIST_TOTAL_KEY));
            result.setSuccess(true);
            result.set("code", 0);
        }
        renderJson(result);
    }

    /**
     * 保存用户信息（新增与修改）
     */
    public void save() {
        String userId = getPara("user_id");
        Integer roleId = getParaToInt("role_id");
        List<SysRoleUser> list = new ArrayList<SysRoleUser>();
        // 多选
        if (userId != null) {
            String[] userIds = userId.split(",");
            for (String uid : userIds) {
                if (uid != null) {
                    SysRoleUser columnModel = new SysRoleUser();
                    columnModel.setRoleId(roleId);
                    columnModel.setUserId(Integer.parseInt(uid));
                    list.add(columnModel);
                }
            }
        }
        Db.batchSave(list, list.size());
        SysLogService.service.setSyslog("角色人员权限", SysLog.STATUS_SUCCESS, "绑定角色人员信息【roleId:" + roleId + ",userId:" + userId + "】成功！");
        renderJson(new Result(true, "保存角色人员信息成功！"));
    }

    /**
     * 删除记录
     */
    public void delete() {
        String[] ids = getParaValues("ids");
        if (ids == null || ids.length <= 0) {
            renderJson(new Result(true, "删除成功！"));
            return;
        }
        boolean b = true;
        for (String id : ids) {
            b = b & SysRoleUser.dao.deleteById(id);
        }
        SysLogService.service.setSyslog("角色人员权限", b ? SysLog.STATUS_SUCCESS : SysLog.STATUS_FAILURE, "删除角色人员信息【id:" + StringUtils.join(ids, ",") + "】" + (b ? "成功" : "失败") + " !");
        renderJson(new Result(b, b ? "删除成功！" : "删除失败！"));
    }

    // ---- 字段关联接口 ----

    /**
     * role_id 字段关联表列表接口
     */
    @ActionKey("/system/role/table2/role_id/sys_role/list")
    public void roleIdSysRoleList() {
        Map<String, Object> params = getPageParamsPlus();
        params.put("_filters_", GeneratorUtils.parseFilterParams(params));
        Object[] values = getParaValues("values");
        Result result = new Result(false, "未知异常！");
        result.set("code", 400);
        if (StringUtils.isNotBlank(values)) { // formSelects的回显接口
            params.put("role_ids", values);
            result.setListData(Db.find(Db.getSqlPara("gid-17-tno-2-cc.jweb.adai.web.system.role.sys_role_user.role_id_sys_role_list", params)));
            result.setSuccess(true);
            result.set("code", 0);
        } else {
            result = Db.paginate("gid-17-tno-2-cc.jweb.adai.web.system.role.sys_role_user.role_id_sys_role_list", "gid-17-tno-2-cc.jweb.adai.web.system.role.sys_role_user.role_id_sys_role_count", params);
            result.set("count", result.get(Result.LIST_TOTAL_KEY));
            result.setSuccess(true);
            result.set("code", 0);
        }
        renderJson(result);
    }

    /**
     * user_id 字段关联表列表接口
     */
    @ActionKey("/system/role/table2/user_id/sys_user/list")
    public void userIdSysUserList() {
        Map<String, Object> params = getPageParamsPlus();
        params.put("_filters_", GeneratorUtils.parseFilterParams(params));
        Object[] values = getParaValues("values");
        Result result = new Result(false, "未知异常！");
        result.set("code", 400);
        if (StringUtils.isNotBlank(values)) { // formSelects的回显接口
            params.put("user_ids", values);
            result.setListData(Db.find(Db.getSqlPara("gid-17-tno-2-cc.jweb.adai.web.system.role.sys_role_user.user_id_sys_user_list", params)));
            result.setSuccess(true);
            result.set("code", 0);
        } else {
            result = Db.paginate("gid-17-tno-2-cc.jweb.adai.web.system.role.sys_role_user.user_id_sys_user_list", "gid-17-tno-2-cc.jweb.adai.web.system.role.sys_role_user.user_id_sys_user_count", params);
            result.set("count", result.get(Result.LIST_TOTAL_KEY));
            result.setSuccess(true);
            result.set("code", 0);
        }
        renderJson(result);
    }

}