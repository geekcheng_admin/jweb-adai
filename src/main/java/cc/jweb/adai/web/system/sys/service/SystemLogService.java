//package cc.jweb.adai.web.system.service;
//
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import com.jfinal.core.JFinal;
//import com.jfinal.plugin.activerecord.Db;
//import com.jfinal.plugin.activerecord.Record;
//import com.ndasec.base.common.lang.StringUtils;
//
//
//public class SystemLogService {
//
//	public static final SystemLogService service = new SystemLogService();
//
//	public void reloadServletContentURLS(){
//		List<Record> resources = Db.find("select * from system_resource");
//		Map<String,Record> URLS = new HashMap<String,Record>();
//		for(Record r : resources){
//			if(StringUtils.isNotBlank(r.getStr("resource_urls"))){
//				String[] urls = r.getStr("resource_urls").split(",");
//				for(String url : urls){
//					URLS.put(url, r);
//				}
//			}
//		}
//		JFinal.me().getServletContext().setAttribute("URLS", URLS);
//	}
//}
