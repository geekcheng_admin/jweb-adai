/*
 * Copyright (c) 2020-2021 imlzw@vip.qq.com jweb.cc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cc.jweb.adai.web.system.file.model;

import cc.jweb.boot.annotation.Column;
import io.jboot.db.annotation.Table;

import java.util.Date;

/**
 * 文件
 * 该模型代码由代码生成器自动生成
 *
 * @author imlzw
 * @generateDate 2020-10-30 13:43:50
 * @source 阿呆极速开发平台 adai.imlzw.cn
 * @since 1.0
 */
@Table(tableName = "sys_file", primaryKey = "file_id")
public class SysFile extends com.jfinal.plugin.activerecord.Model<SysFile> {
    public static final SysFile dao = new SysFile().dao();
    private static final long serialVersionUID = -1604036630984L;
    // 自动生成模型字段列表
    // 文件编号
    @Column(field = "file_id")
    private String fileId;

    // 文件类型
    @Column(field = "file_type")
    private String fileType;

    // 文件名
    @Column(field = "file_name")
    private String fileName;

    // 后缀名
    @Column(field = "file_suffix")
    private String fileSuffix;

    // 文件路径
    @Column(field = "file_path")
    private String filePath;

    // 文件大小
    @Column(field = "file_size")
    private Long fileSize;

    // 上传时间
    @Column(field = "create_datetime")
    private Date createDatetime;

    // 自动生成getter与setter方法

    /**
     * 获取文件编号
     */
    public String getFileId() {
        return (String) get("file_id");
    }

    /**
     * 设置文件编号
     */
    public SysFile setFileId(String fileId) {
        set("file_id", fileId);
        return this;
    }

    /**
     * 获取文件类型
     */
    public String getFileType() {
        return (String) get("file_type");
    }

    /**
     * 设置文件类型
     */
    public SysFile setFileType(String fileType) {
        set("file_type", fileType);
        return this;
    }

    /**
     * 获取文件名
     */
    public String getFileName() {
        return (String) get("file_name");
    }

    /**
     * 设置文件名
     */
    public SysFile setFileName(String fileName) {
        set("file_name", fileName);
        return this;
    }

    /**
     * 获取后缀名
     */
    public String getFileSuffix() {
        return (String) get("file_suffix");
    }

    /**
     * 设置后缀名
     */
    public SysFile setFileSuffix(String fileSuffix) {
        set("file_suffix", fileSuffix);
        return this;
    }

    /**
     * 获取文件路径
     */
    public String getFilePath() {
        return (String) get("file_path");
    }

    /**
     * 设置文件路径
     */
    public SysFile setFilePath(String filePath) {
        set("file_path", filePath);
        return this;
    }

    /**
     * 获取文件大小
     */
    public Long getFileSize() {
        return (Long) get("file_size");
    }

    /**
     * 设置文件大小
     */
    public SysFile setFileSize(Long fileSize) {
        set("file_size", fileSize);
        return this;
    }

    /**
     * 获取上传时间
     */
    public Date getCreateDatetime() {
        return (Date) get("create_datetime");
    }

    /**
     * 设置上传时间
     */
    public SysFile setCreateDatetime(Date createDatetime) {
        set("create_datetime", createDatetime);
        return this;
    }

}