/*
 * Copyright (c) 2020-2021 imlzw@vip.qq.com jweb.cc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cc.jweb.adai.web.system.role.model;

import cc.jweb.boot.annotation.Column;
import com.jfinal.plugin.activerecord.IBean;
import io.jboot.db.annotation.Table;
import io.jboot.db.model.JbootModel;

import java.util.Date;

/**
 * 角色
 * 该模型代码由代码生成器自动生成
 *
 * @author imlzw
 * @generateDate 2020-09-30 11:24:45
 * @source 阿呆极速开发平台 adai.imlzw.cn
 * @since 1.0
 */
@Table(tableName = "sys_role", primaryKey = "role_id")
public class SysRole extends JbootModel<SysRole> implements IBean {
    public static final SysRole dao = new SysRole().dao();
    private static final long serialVersionUID = -1601436285008L;
    // 自动生成模型字段列表

    // 上级角色
    @Column(field = "role_pid")
    private Integer rolePid;

    // 创建时间
    @Column(field = "create_datetime")
    private Date createDatetime;

    // 角色名称
    @Column(field = "role_name")
    private String roleName;

    // 角色关键字
    @Column(field = "role_key")
    private String roleKey;

    // 角色编号
    @Column(field = "role_id")
    private Integer roleId;

    // 自动生成getter与setter方法

    /**
     * 获取上级角色
     */
    public Integer getRolePid() {
        return (Integer) get("role_pid");
    }

    /**
     * 设置上级角色
     */
    public SysRole setRolePid(Integer rolePid) {
        set("role_pid", rolePid);
        return this;
    }

    /**
     * 获取创建时间
     */
    public Date getCreateDatetime() {
        return (Date) get("create_datetime");
    }

    /**
     * 设置创建时间
     */
    public SysRole setCreateDatetime(Date createDatetime) {
        set("create_datetime", createDatetime);
        return this;
    }

    /**
     * 获取角色名称
     */
    public String getRoleName() {
        return (String) get("role_name");
    }

    /**
     * 设置角色名称
     */
    public SysRole setRoleName(String roleName) {
        set("role_name", roleName);
        return this;
    }

    /**
     * 获取角色名称
     */
    public String getRoleKey() {
        return (String) get("role_key");
    }

    /**
     * 设置角色名称
     */
    public SysRole setRoleKey(String roleKey) {
        set("role_key", roleKey);
        return this;
    }

    /**
     * 获取角色编号
     */
    public Integer getRoleId() {
        return (Integer) get("role_id");
    }

    /**
     * 设置角色编号
     */
    public SysRole setRoleId(Integer roleId) {
        set("role_id", roleId);
        return this;
    }

}