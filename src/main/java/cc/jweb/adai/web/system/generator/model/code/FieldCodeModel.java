/*
 * Copyright (c) 2020-2021 imlzw@vip.qq.com jweb.cc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cc.jweb.adai.web.system.generator.model.code;

import cc.jweb.adai.web.system.generator.model.FieldModel;
import cc.jweb.boot.utils.gson.GsonUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 字段代码模型
 */
public class FieldCodeModel extends FieldModel<FieldCodeModel> {
    // 显示，搜索，表单
    private boolean isTitle; // 是否是标题列表
    private boolean showInForm; // 是否在编辑表单显示
    private boolean showInSearchForm; // 是否在查询表单显示
    private String searchMethod; // 查询方式
    private String formComponent; // 表单组件
    private boolean showInList; // 是否在列表表格中显示？
    private boolean isRequired; // 是否在列表表格中显示？
    private Integer formOrder; // 表格排序号
    private List<String> checkers; // 字段校验正则

    public boolean isTitle() {
        return isTitle;
    }

    public FieldCodeModel setTitle(boolean title) {
        this.isTitle = title;
        return this;
    }

    public boolean isShowInForm() {
        return showInForm;
    }

    public FieldCodeModel setShowInForm(boolean showInForm) {
        this.showInForm = showInForm;
        return this;
    }

    public boolean isShowInSearchForm() {
        return showInSearchForm;
    }

    public FieldCodeModel setShowInSearchForm(boolean showInSearchForm) {
        this.showInSearchForm = showInSearchForm;
        return this;
    }

    public boolean isShowInList() {
        return showInList;
    }

    public FieldCodeModel setShowInList(boolean showInList) {
        this.showInList = showInList;
        return this;
    }

    public Integer getFormOrder() {
        return formOrder;
    }

    public FieldCodeModel setFormOrder(Integer formOrder) {
        this.formOrder = formOrder;
        return this;
    }

    public String getSearchMethod() {
        return searchMethod;
    }

    public FieldCodeModel setSearchMethod(String searchMethod) {
        this.searchMethod = searchMethod;
        return this;
    }

    public String getFormComponent() {
        return formComponent;
    }

    public FieldCodeModel setFormComponent(String formComponent) {
        this.formComponent = formComponent;
        return this;
    }

    public boolean isRequired() {
        return isRequired;
    }

    public FieldCodeModel setRequired(boolean required) {
        isRequired = required;
        return this;
    }

    public List<String> getCheckers() {
        return checkers;
    }

    public FieldCodeModel setCheckers(List<String> checkers) {
        this.checkers = checkers;
        return this;
    }

    /**
     * 解析校验器
     *
     * @return
     */
    public List<Map<String, String>> parserCheckers() {
        if (checkers == null || checkers.size() <= 0) {
            return null;
        }
        List list = new ArrayList(checkers.size());
        Pattern pattern = Pattern.compile("(.*?)【(.*?)】");
        for (String checker : checkers) {
            Matcher matcher = pattern.matcher(checker);
            if (matcher.find()) {
                if (matcher.groupCount() >= 2) {
                    Map<String, String> checkerMap = new HashMap<>(2);
                    checkerMap.put("regx", matcher.group(1));
                    checkerMap.put("title", matcher.group(2));
                    list.add(checkerMap);
                }
            }
        }
        return list;
    }

    public String toJson() {
        return GsonUtils.get().toJson(this);
    }


    public void init(FieldModel source) {
        this.setFieldConfig(source.getFieldConfig());
        this.setDecimalPoint(source.getDecimalPoint());
        this.setFieldKey(source.getFieldKey());
        this.setFieldLength(source.getFieldLength());
        this.setFieldOrder(source.getFieldOrder());
        this.setFieldType(source.getFieldType());
        this.setFieldName(source.getFieldName());
        this.setNullable(source.isNullable());
        this.setPrimary(source.isPrimary());
        this.setTableId(source.getTableId());
        this.setFieldId(source.getFieldId());
        this.setCreateDatetime(source.getCreateDatetime());
        this.setFieldDefault(source.getFieldDefault());
    }
}
