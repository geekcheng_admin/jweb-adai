/*
 * Copyright (c) 2020-2021 imlzw@vip.qq.com jweb.cc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cc.jweb.adai.web.system.generator.model.vo;

import java.util.List;

/**
 * 字段模型配置
 */
public class FieldConfig {

    public static final String RELEASE_NONE = "none";
    public static final String RELEASE_TABLE = "table";
    public static final String RELEASE_DIC = "dic";
    public static final String RELEASE_ENUM = "enum";
    public static final String RELEASE_SQL = "sql";

    private List valueCheckers;        // 值检查器集合
    private String releaseType = RELEASE_NONE;    // none.无关联 table:表外键关联 dic:字典关联 enum:固定枚举关联 sql：自定义sql查询
    private Config releaseConfig = new Config();           // 关联配置：表名，字典代码，枚举列表，sql查询, id字段， 标题字段
    private String displayProcesser;        // 显示值处理类
    private Config displayConfig = new Config();;           // 处理参数：表名，字典代码，枚举
    private boolean isAutoIncrease = false; // 是否为自动自增

    public List getValueCheckers() {
        return valueCheckers;
    }

    public FieldConfig setValueCheckers(List valueCheckers) {
        this.valueCheckers = valueCheckers;
        return this;
    }

    public String getReleaseType() {
        return releaseType;
    }

    public FieldConfig setReleaseType(String releaseType) {
        this.releaseType = releaseType;
        return this;
    }

    public Config getReleaseConfig() {
        return releaseConfig;
    }

    public FieldConfig setReleaseConfig(Config releaseConfig) {
        this.releaseConfig = releaseConfig;
        return this;
    }

    public String getDisplayProcesser() {
        return displayProcesser;
    }

    public FieldConfig setDisplayProcesser(String displayProcesser) {
        this.displayProcesser = displayProcesser;
        return this;
    }

    public Config getDisplayConfig() {
        return displayConfig;
    }

    public FieldConfig setDisplayConfig(Config displayConfig) {
        this.displayConfig = displayConfig;
        return this;
    }

    public boolean isAutoIncrease() {
        return isAutoIncrease;
    }

    public FieldConfig setAutoIncrease(boolean autoIncrease) {
        isAutoIncrease = autoIncrease;
        return this;
    }

//    public Config getReleaseTypeConfig(String releaseType) {
//        Config config = new Config();
//        switch (releaseType) {
//            case RELEASE_TABLE:
//                config.addConfig(new ConfigElement("关联表名", "table", "", false, true));
//                break;
//            case RELEASE_DIC:
//                break;
//            case RELEASE_ENUM:
//                break;
//            case RELEASE_SQL:
//                break;
//            case RELEASE_NONE:
//            default:
//                break;
//        }
//        return config;
//    }
}

