
/*
 * Copyright (c) 2020-2021 imlzw@vip.qq.com jweb.cc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cc.jweb.adai.web.system.org.controller;

import cc.jweb.adai.web.system.log.service.SysLogService;
import cc.jweb.adai.web.system.org.model.SysOrg;
import cc.jweb.adai.web.system.sys.model.SysLog;
import cc.jweb.boot.common.exception.ParameterValidationException;
import com.jfinal.core.ActionKey;
import com.jfinal.core.NotAction;
import cc.jweb.boot.common.lang.Result;
import cc.jweb.boot.utils.lang.StringUtils;
import cc.jweb.boot.controller.JwebController;
import cc.jweb.boot.db.Db;
import cc.jweb.adai.web.system.generator.utils.GeneratorUtils;
import cc.jweb.boot.security.annotation.Logical;
import cc.jweb.boot.security.annotation.RequiresPermissions;
import io.jboot.web.controller.annotation.RequestMapping;

import java.util.Date;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 组织机构
 * 该控制器代码由阿呆代码生成器自动生成
 *
 * @generateDate 2020-12-28 18:45:27
 * @since 1.0
 * @author imlzw
 * @source 阿呆极速开发平台 adai.imlzw.cn
 */
@RequiresPermissions("generator:13:view")
@RequestMapping(value = "/system/org/table1", viewPath = "/WEB-INF/views/cc/jweb/jweb/web/system/org/gid_13")
public class Table1SysOrgController extends JwebController {

    public void index() {
        render("index.html");
    }
    /**
     * 编辑页面
     */
    @RequiresPermissions("generator:13:edit")
    public void editPage() {
        String id = getPara("org_id");
        SysOrg sysOrg = null;
        if(id != null) {
            sysOrg = SysOrg.dao.findById(id);
            if (sysOrg == null) {
                sysOrg = new SysOrg();
            }
        }
        if(sysOrg == null) {
            sysOrg = new SysOrg();
        }
        setAttr("detail", sysOrg);
        keepPara();
        render("table1edit.html");
    }

    /**
     * 加载分页列表数据
     */
    @RequiresPermissions("generator:13:list")
    public void list() {
        Map<String, Object> params = getPageParamsPlus();
        params.put("_filters_", GeneratorUtils.parseFilterParams(params));
        Object[] values = getParaValues("values");
        Result result = new Result(false,"未知异常！");
        result.set("code", 400);
        if (StringUtils.isNotBlank(values)){ // formSelects的回显接口
            params.put("_values_",values);
            String valueKey = getPara("valueKey");
            params.put("_valueKey_", valueKey!=null?valueKey:"org_id");
            result.setListData(Db.find(Db.getSqlPara("gid-13-tno-1-cc.jweb.adai.web.system.org.sys_org.queryPageList", params)));
            result.setSuccess(true);
            result.set("code", 0);
        } else {
            result = Db.paginate("gid-13-tno-1-cc.jweb.adai.web.system.org.sys_org.queryPageList", "gid-13-tno-1-cc.jweb.adai.web.system.org.sys_org.count", params);
            result.set("count", result.get(Result.LIST_TOTAL_KEY));
            result.setSuccess(true);
            result.set("code", 0);
        }
        renderJson(result);
    }

    /**
     * 保存组织机构信息（新增与修改）
     */
    @RequiresPermissions(value = {"generator:13:add","generator:13:edit"}, logical = Logical.OR)
    public void save() {
        SysOrg columnModel = getColumnModel(SysOrg.class);
        SysOrg oldModel = null;
        Object id = columnModel.get("org_id");
        if (id != null) {
            oldModel = SysOrg.dao.findById(id);
        }
        if (oldModel != null) { // 编辑
            fixModel(columnModel);
            verifyModel(columnModel);
            columnModel.update();
        } else { // 新增

            columnModel.set("create_datetime", new Date());
            fixModel(columnModel);
            verifyModel(columnModel);
            columnModel.save();
        }
        SysLogService.service.setSyslog("组织机构", SysLog.STATUS_SUCCESS ,"保存组织机构【" + columnModel.getOrgName()+ "】成功！");
        renderJson(new Result(true, "保存组织机构信息成功！"));
    }

    /**
     *  修正模型数据
     *  避免模型非空字段 无数据时，保存或者更新时异常
     */
    @NotAction
    private void fixModel(SysOrg columnModel) {
        Set<String> attrs = columnModel._getAttrsEntrySet().stream().map(stringObjectEntry -> stringObjectEntry.getKey()).collect(Collectors.toSet());
        if(attrs.contains("org_id") && columnModel.get("org_id")==null){
            // 字段默认值为：NULL
            columnModel.remove("org_id");
        }

        if(attrs.contains("org_pid") && columnModel.get("org_pid")==null){
            // 字段默认值为：0
            columnModel.set("org_pid", "0");
        }

        if(attrs.contains("org_code") && columnModel.get("org_code")==null){
            // 字段默认值为：NULL
            columnModel.remove("org_code");
        }

        if(attrs.contains("org_name") && columnModel.get("org_name")==null){
            // 字段默认值为：NULL
            columnModel.remove("org_name");
        }

        if(attrs.contains("create_datetime") && columnModel.get("create_datetime")==null){
            // 字段默认值为：NULL
            columnModel.remove("create_datetime");
        }

        if(attrs.contains("order_no") && columnModel.get("order_no")==null){
            // 字段默认值为：NULL
            columnModel.remove("order_no");
        }

    }

    /**
     *  验证数据
     *  对模型中的字段数据进行校验
     */
    @NotAction
    private void verifyModel(SysOrg columnModel) {
        Set<String> attrs = columnModel._getAttrsEntrySet().stream().map(stringObjectEntry -> stringObjectEntry.getKey()).collect(Collectors.toSet());
        if(attrs.contains("org_code") && columnModel.getOrgCode()!=null && columnModel.getOrgCode().length() > 32){
            throw new ParameterValidationException("机构代码的长度不能超过32个字符！");
        }
        if(attrs.contains("org_name") && columnModel.getOrgName()!=null && columnModel.getOrgName().length() > 128){
            throw new ParameterValidationException("机构名称的长度不能超过128个字符！");
        }
    }

    /**
     * 删除记录
     */
    @RequiresPermissions("generator:13:del")
    public void delete() {
        String[] ids = getParaValues("ids");
        if (ids == null || ids.length <= 0) {
            renderJson(new Result(true, "删除成功！"));
            return;
        }
        boolean b = true;
        for(String id : ids) {
            b = b & SysOrg.dao.deleteById(id);
         }
        SysLogService.service.setSyslog("组织机构", b ? SysLog.STATUS_SUCCESS : SysLog.STATUS_FAILURE, "删除组织机构【id:" + StringUtils.join(ids, ",") + "】" + (b ? "成功" : "失败") + " !");
        renderJson(new Result(b, b ? "删除成功！" : "删除失败！"));
    }

    // ---- 字段关联接口 ----
    /**
     * org_pid 字段关联表列表接口
     */
    @ActionKey("/system/org/table1/org_pid/sql/list")
    public void orgPidSqlList() {
        Map<String, Object> params = getPageParamsPlus();
        params.put("_filters_", GeneratorUtils.parseFilterParams(params));
        Object[] values = getParaValues("values");
        Result result = new Result(false,"未知异常！");
        result.set("code", 400);
        if (StringUtils.isNotBlank(values)){ // formSelects的回显接口
            params.put("org_ids",values);
            result.setListData(Db.find(Db.getSqlPara("gid-13-tno-1-cc.jweb.adai.web.system.org.sys_org.org_pid_sql_list", params)));
            result.setSuccess(true);
            result.set("code", 0);
        } else {
            result = Db.paginate("gid-13-tno-1-cc.jweb.adai.web.system.org.sys_org.org_pid_sql_list", "gid-13-tno-1-cc.jweb.adai.web.system.org.sys_org.org_pid_sql_count", params);
            result.set("count", result.get(Result.LIST_TOTAL_KEY));
            result.setSuccess(true);
            result.set("code", 0);
        }
        renderJson(result);
    }

}