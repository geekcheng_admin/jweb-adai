/*
 * Copyright (c) 2020-2021 imlzw@vip.qq.com jweb.cc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cc.jweb.adai.web.system.generator.service.dataprocess;


import cc.jweb.adai.web.system.generator.model.vo.Config;
import cc.jweb.adai.web.system.generator.model.vo.ConfigParam;

/**
 * 数据处理插件接口
 * @author linzhiwei
 *
 */
public interface DataProcess extends ConfigParam {

	/**
	 * 处理对象
	 * @param object
	 * @return
	 */
	public <T> T process(Object object, Config config) throws Exception;

	/**
	 * 获取配置描述信息
	 * @return
	 */
	public String getConfigDesc();
}
