/*
 * Copyright (c) 2020-2021 imlzw@vip.qq.com jweb.cc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cc.jweb.adai.web.system.dic.controller;

import cc.jweb.boot.common.lang.Result;
import cc.jweb.boot.controller.JwebController;
import cc.jweb.boot.db.Db;
import cc.jweb.boot.utils.lang.StringUtils;
import cc.jweb.adai.web.system.generator.utils.GeneratorUtils;
import io.jboot.web.controller.annotation.RequestMapping;

import java.util.Map;

/**
 * 标准双模型列表模板主页
 * 该控制器代码由阿呆代码生成器自动生成
 *
 * @author imlzw
 * @generateDate 2020-09-27 18:06:00
 * @source 阿呆极速开发平台 adai.imlzw.cn
 * @since 1.0
 */
@RequestMapping(value = "/system/dic/list", viewPath = "/WEB-INF/views/cc/jweb/web/system/dic/gid_6")
public class ListController extends JwebController {

    /**
     * 加载分页列表数据
     */
    public void index() {
        Map<String, Object> params = getPageParamsPlus();
        params.put("_filters_", GeneratorUtils.parseFilterParams(params));
        Object[] values = getParaValues("values");
        Result result = new Result(false, "未知异常！");
        result.set("code", 400);
        if (StringUtils.isNotBlank(values)) { // formSelects的回显接口
            params.put("_values_", values);
            String valueKey = getPara("valueKey");
            params.put("_valueKey_", valueKey != null ? valueKey : "dic_id");
            result.setListData(Db.find(Db.getSqlPara("system.sys_dic.queryPageList", params)));
            result.setSuccess(true);
            result.set("code", 0);
        } else {
            result = Db.paginate("system.sys_dic.queryPageList", "system.sys_dic.count", params);
            result.set("count", result.get(Result.LIST_TOTAL_KEY));
            result.setSuccess(true);
            result.set("code", 0);
        }
        renderJson(result);
    }
}