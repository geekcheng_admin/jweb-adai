/*
 * Copyright (c) 2020-2021 imlzw@vip.qq.com jweb.cc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cc.jweb.adai.web.system.generator.model;

import cc.jweb.boot.annotation.Column;
import cc.jweb.boot.utils.gson.GsonUtils;
import cc.jweb.boot.utils.lang.collection.MapUtils;
import com.jfinal.plugin.activerecord.IBean;
import cc.jweb.adai.web.system.generator.model.vo.Config;
import cc.jweb.adai.web.system.generator.model.vo.ConfigElement;
import io.jboot.db.annotation.Table;
import io.jboot.db.model.JbootModel;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 模板模型
 */
@Table(tableName = "sys_template_model", primaryKey = "template_id")
public class TemplateModel extends JbootModel<TemplateModel> implements IBean {
    public static final TemplateModel dao = new TemplateModel().dao();
    private static final long serialVersionUID = -1515214825779L;
    @Column(field = "template_id")
    private String templateId;

    @Column(field = "official_id")
    private String official_id;

    @Column(field = "template_name")
    private String templateName;

    @Column(field = "template_key")
    private String templateKey;

    @Column(field = "create_datetime")
    private Date createDatetime;

    @Column(field = "table_require_count")
    private int tableRequireCount; // 需要的表数据

    @Column(field = "template_config")
    private String templateConfig;

    @Column(field = "thumb")
    private String thumb;

    @Column(field = "desc")
    private String desc;

    @Column(field = "version")
    private String version;

    @Column(field = "author")
    private String author;

    @Column(field = "is_system")
    private boolean isSystem;

    @Column(field = "order_no")
    private int orderNo;

    public String getTemplateId() {
        return get("template_id");
    }

    public TemplateModel setTemplateId(String templateId) {
        set("template_id", templateId);
        return this;
    }

    public String getTemplateName() {
        return get("template_name");
    }

    public TemplateModel setTemplateName(String templateName) {
        set("template_name", templateName);
        return this;
    }

    public String getTemplateKey() {
        return get("template_key");
    }

    public TemplateModel setTemplateKey(String templateKey) {
        set("template_key", templateKey);
        return this;
    }

    public Date getCreateDatetime() {
        return get("create_datetime");
    }

    public TemplateModel setCreateDatetime(Date createDatetime) {
        set("create_datetime", createDatetime);
        return this;
    }

    public String getTemplateConfig() {
        return get("template_config");
    }

    public TemplateModel setTemplateConfig(String templateConfig) {
        set("template_config", templateConfig);
        return this;
    }

    public String getThumb() {
        return get("thumb");
    }

    public TemplateModel setThumb(String thumb) {
        set("thumb", thumb);
        return this;
    }

    public String getDesc() {
        return get("desc");
    }

    public TemplateModel setDesc(String desc) {
        set("desc", desc);
        return this;
    }

    public String getAuthor() {
        return get("author");
    }

    public TemplateModel setAuthor(String author) {
        set("author", author);
        return this;
    }

    public String getVersion() {
        return get("version");
    }

    public TemplateModel setVersion(String version) {
        set("version", version);
        return this;
    }

    public Integer getOrderNo() {
        return get("orderNo");
    }

    public TemplateModel setOrderNo(Integer orderNo) {
        set("order_no", orderNo);
        return this;
    }

    public boolean isSystem() {
        return MapUtils.getBoolean(this._getAttrs(), "is_system");
    }

    public TemplateModel setIsSystem(boolean isSystem) {
        set("is_system", isSystem ? 1 : 0);
        return this;
    }

    public int getTableRequireCount() {
        return get("table_require_count");
    }

    public TemplateModel setTableRequireCount(int tableRequireCount) {
        set("table_require_count", tableRequireCount);
        return this;
    }

    public String getOfficialId() {
        return get("official_id");

    }

    public TemplateModel setOfficialId(String officialId) {
        set("official_id", officialId);
        return this;
    }

    public Config toTemplateConfig() {
        List<Map> mapList = GsonUtils.get().fromJson(getTemplateConfig(), List.class);
        Config config = new Config();
        for (Map map : mapList) {
            config.addConfig(new ConfigElement((String) map.get("title"), (String) map.get("name"), (String) map.get("value"), (String) map.get("desc")));
        }
        return config;
    }
}
