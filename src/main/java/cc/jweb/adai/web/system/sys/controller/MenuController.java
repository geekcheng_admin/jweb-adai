/*
 * Copyright (c) 2020-2021 imlzw@vip.qq.com jweb.cc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cc.jweb.adai.web.system.sys.controller;

import cc.jweb.boot.common.lang.Result;
import cc.jweb.boot.controller.JwebController;
import cc.jweb.boot.db.Db;
import cc.jweb.boot.security.utils.JwebSecurityUtils;
import com.jfinal.plugin.activerecord.Record;
import io.jboot.Jboot;
import io.jboot.web.controller.annotation.RequestMapping;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RequestMapping(value = "system/menu", viewPath = "/WEB-INF/views/system/user")
public class MenuController extends JwebController {


    /**
     * 加载分页列表数据
     */
    public void index() throws IOException {
        String isTest = Jboot.configValue("test", "false");
        if ("true".equalsIgnoreCase(isTest)) {
            render("/assets/test_menu.json");
            return;
        }
        Map<String, Object> params = getPageParams();
        String uid = JwebSecurityUtils.getAccount().getUid();
        params.put("oper_type", 0);// 菜单类型
        Result result = new Result(true);
        List<Record> records = Db.find("select distinct(so.oper_id), so.* from sys_oper so, sys_role_oper sro, sys_role_user sru where sro.role_id = sru.role_id and sro.oper_id = so.oper_id and so.oper_type = 0  and sru.user_id = ? order by so.order_no asc", uid);
        result.set("code", result.isSuccess() ? 0 : "500");
        result.set("msg", result.getMessage());
        result.set("data", cover2MenuData(records, 0));
        renderJson(result);
    }

    private List<Map> cover2MenuData(List<Record> listData, int pid) {
        List<Map> list = new ArrayList<>();
        for (Record item : listData) {
            if (pid == (Integer) item.get("oper_pid")) {
                Map data = new HashMap();
                data.put("name", item.get("oper_key"));
                data.put("title", item.get("oper_name"));
                data.put("icon", item.get("oper_icon"));
                Object operPath = item.get("oper_path");
                if (operPath != null) {
                    String path = operPath.toString();
//                    if (path.startsWith("/#")) {
//                        data.put("jump", path.replace("/#", ""));
//                    } else {
                        data.put("jump", operPath);
//                    }
                }
                List<Map> children = cover2MenuData(listData, (Integer) item.get("oper_id"));
                if (children != null && children.size() > 0) {
                    data.put("list", children);
                    data.put("spread", true);
                }
                list.add(data);
            }
        }
        return list;
    }


    /**
     * {
     *   "code": 0,
     *   "msg": "",
     *   "data": [
     *     {
     *       "name": "system",
     *       "title": "系统设置",
     *       "spread": true,
     *       "list": [
     *         {
     *           "name": "usermgr",
     *           "title": "用户管理",
     *           "jump": "/_/system/user/"
     *         },
     */


}
