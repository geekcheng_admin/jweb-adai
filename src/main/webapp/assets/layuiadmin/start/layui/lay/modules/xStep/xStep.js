﻿layui.define(['layer', 'carousel', 'xUtils'], function (exports) {
    var $ = layui.jquery;
    var layer = layui.layer;
    var carousel = layui.carousel;
    var xUtils = layui.xUtils;

    var XStep = function () {
        this.version = '1.0';
        this.base = '';
    }

    // 添加步骤条dom节点
    var renderDom = function (options) {
        let elem = options.elem, stepItems = options.stepItems, focusIndex = options.focusIndex;
        var stepDiv = '<div class="lay-xstep">';
        for (var i = 0; i < stepItems.length; i++) {
            stepDiv += '<div class="step-item">';
            // 线
            if (i < (stepItems.length - 1)) {
                if (i < focusIndex) {
                    stepDiv += '<div class="step-item-tail"><i class="step-item-tail-done"></i></div>';
                } else {
                    stepDiv += '<div class="step-item-tail"><i class=""></i></div>';
                }
            }

            // 数字
            var number = stepItems[i].number;
            if (!number) {
                number = i + 1;
            }
            stepDiv += `<div class="step-item-head ${i == focusIndex ? 'step-item-head-active' : ''} "><i class="layui-icon ${i < focusIndex ? 'layui-icon-ok' : ''}" n="${number}">${i >= focusIndex ? number : ''}</i></div>`;

            // 标题和描述
            var title = stepItems[i].title;
            var desc = stepItems[i].desc;
            if (title || desc) {
                stepDiv += '<div class="step-item-main">';
                if (title) {
                    stepDiv += '<div class="step-item-main-title">' + title + '</div>';
                }
                if (desc) {
                    stepDiv += '<div class="step-item-main-desc">' + desc + '</div>';
                }
                stepDiv += '</div>';
            }
            stepDiv += '</div>';
        }
        stepDiv += '</div>';

        $(elem).prepend(stepDiv);

        // 计算每一个条目的宽度
        var bfb = 100 / stepItems.length;
        $(options.elem + ' .step-item').css('width', bfb + '%');
        $(options.elem + " .lay-xstep .step-item").eq(0).css('margin-left', "calc(" + (bfb / 2 + '%') + " - 14px)");
    };

    // 渲染步骤条
    XStep.prototype.render = function (options) {
        let that = this;
        let base = layui.cache.modules.xStep.replace("xStep.js", "") || '';
        this.base = base;
        xUtils.loadCss(base + "xStep.css");
        options.indicator = 'none';  // 不显示指示器
        options.arrow = 'always';  // 始终显示箭头
        options.autoplay = false;  // 关闭自动播放

        // 渲染步骤条
        var stepItems = options.stepItems;
        options.focusIndex = 0;
        this.step(options);

        return {
            options: options,
            next: function () {
                that.next(options);
            },
            pre: function () {
                that.pre(options)
            },
            hasPre: function () {
                return options.focusIndex > 0;
            },
            hasNext: function () {
                return options.focusIndex < options.stepItems.length - 1;
            }
        }
    }

    // 上一步
    XStep.prototype.pre = function (options) {
        let postion = options.focusIndex - 1;
        let overflow = postion < 0;
        if (overflow) {
            return false;
        }
        options.focusIndex = overflow ? 0 : postion;
        this.step(options);
        return true;
    };

    // 下一步
    XStep.prototype.next = function (options) {
        let postion = options.focusIndex + 1;
        let overflow = postion >= options.stepItems.length;
        if (overflow) {
            return false;
        }
        options.focusIndex = postion;
        this.step(options);
        return true;
    }

    // 定位步骤
    XStep.prototype.step = function (options) {
        let position = options.focusIndex || 0;
        if (!$(options.elem + ' .lay-xstep')[0]) {
            renderDom(options);
        } else {
            let $i = $(options.elem + ' .lay-xstep .step-item-head i');
            for (var i = 0; i < $i.length; i++) {
                if (i >= position) {
                    $i.eq(i).html($i.eq(i).attr("n"));
                } else {
                    $i.eq(i).html("");
                }
            }
            $(options.elem + ' .lay-xstep').find(`.step-item:gt(${position})`).find(".step-item-head").removeClass("step-item-head-active").find("i").removeClass("layui-icon-ok");
            $(options.elem + ' .lay-xstep').find(`.step-item:lt(${position})`).find(".step-item-head").removeClass("step-item-head-active").find("i").addClass("layui-icon-ok");
            $(options.elem + ' .lay-xstep').find(`.step-item:eq(${position})`).find(".step-item-head").addClass("step-item-head-active").find("i").removeClass("layui-icon-ok");
        }
        options.stepAction && options.stepAction(options.stepItems[options.focusIndex]);
    };
    exports('xStep', new XStep());
});
